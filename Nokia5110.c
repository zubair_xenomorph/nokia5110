
#include "stdio.h"
#include <stdint.h>
//#include "lm4f120h5qr.h"
#include "Nokia5110.h"
#define DC                      (*((volatile uint32_t *)0x40004100))
#define DC_COMMAND              0
#define DC_DATA                 0x40
#define RESET                   (*((volatile uint32_t *)0x40004200))
#define RESET_LOW               0
#define RESET_HIGH              0x80
#define SYSCTL_RCGC2_R          (*((volatile unsigned long *)0x400FE108))
#define SYSCTL_RCGCSSI_R        (*((volatile unsigned long *)0x400FE61C))
#define SYSCTL_RCGCGPIO_R       (*((volatile unsigned long *)0x400FE608))
#define GPIO_PORTA_DIR_R        (*((volatile uint32_t *)0x40004400))
#define GPIO_PORTA_AFSEL_R      (*((volatile uint32_t *)0x40004420))
#define GPIO_PORTA_DEN_R        (*((volatile uint32_t *)0x4000451C))
#define GPIO_PORTA_AMSEL_R      (*((volatile uint32_t *)0x40004528))
#define GPIO_PORTA_PCTL_R       (*((volatile uint32_t *)0x4000452C))
#define SSI0_CR0_R              (*((volatile uint32_t *)0x40008000))
#define SSI0_CR1_R              (*((volatile uint32_t *)0x40008004))
#define SSI0_DR_R               (*((volatile uint32_t *)0x40008008))
#define SSI0_SR_R               (*((volatile uint32_t *)0x4000800C))
#define SSI0_CPSR_R             (*((volatile uint32_t *)0x40008010))
#define SSI0_CC_R               (*((volatile uint32_t *)0x40008FC8))
#define SSI_CR0_SCR_M           0x0000FF00  
#define SSI_CR0_SPH             0x00000080  
#define SSI_CR0_SPO             0x00000040  
#define SSI_CR0_FRF_M           0x00000030  
#define SSI_CR0_FRF_MOTO        0x00000000  
#define SSI_CR0_DSS_M           0x0000000F  
#define SSI_CR0_DSS_8           0x00000007  
#define SSI_CR1_MS              0x00000004  
#define SSI_CR1_SSE             0x00000002  
                                            
#define SSI_SR_BSY              0x00000010  
#define SSI_SR_TNF              0x00000002  
#define SSI_CPSR_CPSDVSR_M      0x000000FF  
#define SSI_CC_CS_M             0x0000000F  
#define SSI_CC_CS_SYSPLL        0x00000000  
                                           


enum typeOfWrite{
  COMMAND,                              
  DATA                                 
};

void static lcdwrite(enum typeOfWrite type, char message){
  if(type == COMMAND){
                                        
    while((SSI0_SR_R&SSI_SR_BSY)==SSI_SR_BSY){};
    DC = DC_COMMAND;
    SSI0_DR_R = message;                
                                        
    while((SSI0_SR_R&SSI_SR_BSY)==SSI_SR_BSY){};
  } else{
    while((SSI0_SR_R&SSI_SR_TNF)==0){}; 
    DC = DC_DATA;
    SSI0_DR_R = message;                
  }
}
uint32_t NokiaLineNumber=0;


void Nokia5110_Init(void){
  volatile uint32_t delay;
  SYSCTL_RCGCSSI_R |= 0x01;  
  SYSCTL_RCGCGPIO_R |= 0x01; 
  delay = SYSCTL_RCGC2_R;               
  GPIO_PORTA_DIR_R |= 0xC0;             
  GPIO_PORTA_AFSEL_R |= 0x2C;           
  GPIO_PORTA_AFSEL_R &= ~0xC0;          
  GPIO_PORTA_DEN_R |= 0xEC;             
                                        
  GPIO_PORTA_PCTL_R = (GPIO_PORTA_PCTL_R&0xFF0F00FF)+0x00202200;
                                        
  GPIO_PORTA_PCTL_R = (GPIO_PORTA_PCTL_R&0x00FFFFFF)+0x00000000;
  GPIO_PORTA_AMSEL_R &= ~0xEC;          
  SSI0_CR1_R &= ~SSI_CR1_SSE;           
  SSI0_CR1_R &= ~SSI_CR1_MS;            
                                        
  SSI0_CC_R = (SSI0_CC_R&~SSI_CC_CS_M)+SSI_CC_CS_SYSPLL;
                                        
  SSI0_CPSR_R = (SSI0_CPSR_R&~SSI_CPSR_CPSDVSR_M)+16;
  SSI0_CR0_R &= ~(SSI_CR0_SCR_M |       
                  SSI_CR0_SPH |         
                  SSI_CR0_SPO);         
                                        
  SSI0_CR0_R = (SSI0_CR0_R&~SSI_CR0_FRF_M)+SSI_CR0_FRF_MOTO;
                                        
  SSI0_CR0_R = (SSI0_CR0_R&~SSI_CR0_DSS_M)+SSI_CR0_DSS_8;
  SSI0_CR1_R |= SSI_CR1_SSE;            

  RESET = RESET_LOW;                    
  for(delay=0; delay<10; delay=delay+1);
  RESET = RESET_HIGH;                   

  lcdwrite(COMMAND, 0x21);              
  lcdwrite(COMMAND, CONTRAST);          
  lcdwrite(COMMAND, 0x04);              
  lcdwrite(COMMAND, 0x14);              

  lcdwrite(COMMAND, 0x20);              
  lcdwrite(COMMAND, 0x0C);              
}
void Output_Init(void){
  Nokia5110_Init();
  NokiaLineNumber=0;
}

void Output_Clear(void){ 
  Nokia5110_Clear();
}

void Output_On(void){    
  Nokia5110_Init();      
}


void Nokia5110_SetCursor(unsigned char newX, unsigned char newY){
  if((newX > 11) || (newY > 5)){        
    return;                             
  }
  lcdwrite(COMMAND, 0x80|(newX*7));     
  lcdwrite(COMMAND, 0x40|newY);         
}


void Nokia5110_Clear(void){
  int i;
  for(i=0; i<(MAX_X*MAX_Y/8); i=i+1){
    lcdwrite(DATA, 0x00);
  }
  Nokia5110_SetCursor(0, 0);
}

void Nokia5110_DrawFullImage(const char *ptr){
  int i;
  Nokia5110_SetCursor(0, 0);
  for(i=0; i<(MAX_X*MAX_Y/8); i=i+1){
    lcdwrite(DATA, ptr[i]);
  }
}
